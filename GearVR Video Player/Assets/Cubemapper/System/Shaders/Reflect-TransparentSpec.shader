// Transparent Reflective Specular is a shader by Amir Abiri, generously donated to the Public Domain (I confirmed)
// The Shader was at the time available here: http://www.asteroidspatrol.com/shaders/Transparent-Reflective-Spec.shader
// I modified the shader to make use of the additional features in the Cubemapper and include it in the package
Shader "Reflective/Cubemapper/Transparent Specular" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 0)
	_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
	_ReflectColor ("Reflection Color", Color) = (1,1,1,0.5)
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_Cube ("Reflection Cubemap", Cube) = "_Skybox" { TexGen CubeReflect }
	_Cube2 ("Reflection Cubemap #2", Cube) = "_Skybox" { TexGen CubeReflect }
	_Blend ("Blend", Range (0, 1) ) = 0.0 
	_Blur ("Blur", Range (0.1,1)) = 1.0
}

SubShader {
	LOD 300
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		
CGPROGRAM
#pragma surface surf BlinnPhong alpha
#pragma glsl
#pragma target 3.0

sampler2D _MainTex;
samplerCUBE _Cube, _Cube2;
float _Blend, _Blur;

fixed4 _Color;
fixed4 _ReflectColor;
half _Shininess;

struct Input {
	float2 uv_MainTex;
	float3 worldRefl;
};

void surf (Input IN, inout SurfaceOutput o) 
{
	const int NumMipmap = 7;
	fixed Blur = (1 - _Blur) * NumMipmap; 

	fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	o.Albedo = tex.rgb * _Color.rgb;
	o.Gloss = tex.a;
	o.Specular = _Shininess;
	
	float4 reflcol = texCUBElod (_Cube, float4(IN.worldRefl, Blur));
	float4 reflcol2 = texCUBElod (_Cube2, float4(IN.worldRefl, Blur));
	reflcol *= tex.a;
	reflcol2 *= tex.a;

	float4 lerpRefl = lerp(reflcol, reflcol2, _Blend);
	o.Emission = lerpRefl.rgb * _ReflectColor.rgb;
	o.Alpha = lerpRefl.a * _Color.a;
}
ENDCG
} 

FallBack "Transparent/Specular"
}
