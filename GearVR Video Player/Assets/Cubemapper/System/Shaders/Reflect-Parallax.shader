Shader "Reflective/Cubemapper/Parallax Diffuse" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_ReflectColor ("Reflection Color", Color) = (1,1,1,0.5)
	_Parallax ("Height", Range (0.005, 0.08)) = 0.02
	_MainTex ("Base (RGB) RefStrength (A)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
	_ParallaxMap ("Heightmap (A)", 2D) = "black" {}
	_Cube ("Reflection Cubemap", Cube) = "_Skybox" { TexGen CubeReflect }
	_Cube2 ("Reflection Cubemap #2", Cube) = "_Skybox" { TexGen CubeReflect }
	_Blend ("Blend", Range (0, 1) ) = 0.0 
	_Blur ("Blur", Range (0.1,1)) = 1.0
}
SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 500
	
CGPROGRAM
#pragma surface surf Lambert
#pragma glsl
#pragma target 3.0
//input limit (8) exceeded, shader uses 9
#pragma exclude_renderers d3d11_9x

sampler2D _MainTex;
sampler2D _BumpMap;
sampler2D _ParallaxMap;
samplerCUBE _Cube, _Cube2;
float _Blend, _Blur;

fixed4 _Color;
fixed4 _ReflectColor;
float _Parallax;

struct Input {
	float2 uv_MainTex;
	float2 uv_BumpMap;
	float3 worldRefl;
	float3 viewDir;
	INTERNAL_DATA
};

void surf (Input IN, inout SurfaceOutput o)
{
	const int NumMipmap = 7;
	fixed Blur = (1 - _Blur) * NumMipmap; 

	half h = tex2D (_ParallaxMap, IN.uv_BumpMap).w;
	float2 offset = ParallaxOffset (h, _Parallax, IN.viewDir);
	IN.uv_MainTex += offset;
	IN.uv_BumpMap += offset;
	
	fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	fixed4 c = tex * _Color;
	o.Albedo = c.rgb;
	
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
	
	float3 worldRefl = WorldReflectionVector (IN, o.Normal);
	float4 reflcol = texCUBElod (_Cube, float4(worldRefl, Blur));
	float4 reflcol2 = texCUBElod (_Cube2, float4(worldRefl, Blur));
	reflcol *= tex.a;
	reflcol2 *= tex.a;

	float4 lerpRefl = lerp(reflcol, reflcol2, _Blend);
	o.Emission = lerpRefl.rgb * _ReflectColor.rgb;
	o.Alpha = lerpRefl.a * _ReflectColor.a;
}
ENDCG
}

FallBack "Reflective/Bumped Diffuse"
}
