/*------------------------------------------------
// CUBEMAPPER
// Version: See Readme
// Created by: Rainer Liessem
// Website: http://www.spreadcamp.com
//
// PLEASE RESPECT THE LICENSE TERMS THAT YOU
// AGREED UPON WITH YOUR PURCHASE OF THIS ASSET
------------------------------------------------*/
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

namespace Cubemapper
{
    public class CubemapGenerator : MonoBehaviour
    {
#if !UNITY_EDITOR
        void Start()
        {
            Destroy(gameObject);
        }
#else
        // Use Method for Unity Basic or Unity Pro?
        public enum GenerateMethod { Basic, Pro };
        public GenerateMethod generateMethod = GenerateMethod.Basic;

        // Status Stuff for the Custom Editor
        public bool generate = false;
        public bool isTakingScreenshots = false;
        public bool completedTakingScreenshots = false;

        // Output Folders
        public string pathCubemaps;
        public string pathCubemapPNG;

        // Cubemap Settings
        public CubemapNode[] nodes = null; // array of nodes to generate cubemaps from
        public bool makePNG = false; // makes editable PNG images if true
        public bool useLinearSpace = false; // Color Space for HDR (also uses different texture format)
        public bool useMipMaps = false; // generates cubemaps with mipmaps if true
        public float mipMapBias = 0.0f;
        public bool smoothEdges = true;
        public int smoothEdgesWidth = 1;
        public TextureFormat textureFormat = TextureFormat.RGB24;

        // Camera Settings
        public CameraClearFlags camClearFlags = CameraClearFlags.Skybox;
        public Color camBGColor = new Color(0.192f, 0.302f, 0.475f, 0.020f);
        public LayerMask cullingMask = -1;
        public float farClipPlane = 1000f;

        // Screenshot Settings
        public int resolution = 32;
        public int nodeResolution = 32; // this is used when nodes override

        // Private members
        private GameObject camObj;
        private Transform camT;
        private Camera cam;
        private enum DIR { Right, Left, Top, Bottom, Front, Back }
        private DIR currentDir = DIR.Right;
        private string sceneName;


        void OnDrawGizmos() { Gizmos.DrawIcon(transform.position, "cManager.png"); }

        void Start()
        {
            // Are we to generate Cubemaps with Unity Basic?
            if (generateMethod == GenerateMethod.Basic)
            {
                if ((generate && Application.isEditor && Application.isPlaying) && !isTakingScreenshots)
                {
                    // Check for existing cameras and disable them to avoid interference
                    DisableOtherCameras();

                    // Create our Cubemapper camera
                    SetupCamera();

                    // Slow time to prevent weird things from happening in dynamic enviroments
                    Time.timeScale = .0000001f;

                    // Get scene name for naming purposes later on
                    sceneName = CMGenerate.GetSceneName(generateMethod);

                    // Start taking Screenshots at our Nodes
                    isTakingScreenshots = true;
                    StartCoroutine("ScreenCapture");
                }
                else
                    Destroy(gameObject);
            }
        }

        void SetupCamera()
        {
            camObj = new GameObject("Cubemap Camera (temporary during Generation)", typeof(Camera));
            camT = camObj.transform;
            cam = camObj.GetComponent<Camera>();
            cam.nearClipPlane = 0.01f;
            cam.fieldOfView = 90;
            cam.aspect = 1.0f;
            cam.cullingMask = cullingMask;
            cam.farClipPlane = farClipPlane;
            cam.clearFlags = camClearFlags;
            cam.backgroundColor = camBGColor;
            cam.hdr = (useLinearSpace) ? true : false;
        }

        /// <summary>
        /// Disables other Cameras in the scene. This function is only useful with the Unity Basic generate method
        /// </summary>
        void DisableOtherCameras()
        {
            Camera[] otherCams = FindObjectsOfType(typeof(Camera)) as Camera[];

            foreach (Camera camera in otherCams)
            {
                //camera.enabled = false;
                camera.gameObject.SetActive(false);
            }
        }

        IEnumerator ScreenCapture()
        {
            while (isTakingScreenshots)
            {
                CMGenerate.ToggleNodePreview(false); // Disable node preview

                // Loop through each node for capture
                for (int a = 0; a < nodes.Length; a++)
                {
                    // Make sure this Node is set to allow generation of either cubemaps or PNGs
                    // Ignore the Allow parameter if we have no cubemap assigned yet at all
                    if (nodes[a].allowCubemapGeneration || nodes[a].allowGeneratePNG || (!nodes[a].allowCubemapGeneration && nodes[a].cubemap == null))
                    {
                        // Position Camera
                        camT.position = nodes[a].transform.position;
                        camT.rotation = Quaternion.identity;

                        // Set resolution because Node may override
                        nodeResolution = (nodes[a].overrideResolution) ? nodes[a].resolution : resolution;

                        // Make cubemap
                        Cubemap cubemap = new Cubemap(nodeResolution, textureFormat, useMipMaps);
                        cubemap.mipMapBias = mipMapBias;

                        // Loop through all Directions to take screenshots for this node
                        for (int b = 0; b < 6; b++)
                        {
                            //Debug.Log("Processing Node " + nodes[a].name + " in Direction " + currentDir);

                            switch (currentDir)
                            {
                                case DIR.Right:
                                    camT.rotation = Quaternion.Euler(0, 90, 0);
                                    yield return StartCoroutine(MakeSnapshot(cubemap, CubemapFace.PositiveX, nodes[a]));
                                    currentDir = DIR.Left;
                                    break;

                                case DIR.Left:
                                    camT.rotation = Quaternion.Euler(0, -90, 0);
                                    yield return StartCoroutine(MakeSnapshot(cubemap, CubemapFace.NegativeX, nodes[a]));
                                    currentDir = DIR.Top;
                                    break;

                                case DIR.Top:
                                    camT.rotation = Quaternion.Euler(-90, 0, 0);
                                    yield return StartCoroutine(MakeSnapshot(cubemap, CubemapFace.PositiveY, nodes[a]));
                                    currentDir = DIR.Bottom;
                                    break;

                                case DIR.Bottom:
                                    camT.rotation = Quaternion.Euler(90, 0, 0);
                                    yield return StartCoroutine(MakeSnapshot(cubemap, CubemapFace.NegativeY, nodes[a]));
                                    currentDir = DIR.Front;
                                    break;

                                case DIR.Front:
                                    camT.rotation = Quaternion.Euler(0, 0, 0);
                                    yield return StartCoroutine(MakeSnapshot(cubemap, CubemapFace.PositiveZ, nodes[a]));
                                    currentDir = DIR.Back;
                                    break;

                                case DIR.Back:
                                    camT.rotation = Quaternion.Euler(0, 180, 0);
                                    yield return StartCoroutine(MakeSnapshot(cubemap, CubemapFace.NegativeZ, nodes[a]));
                                    currentDir = DIR.Right; // back to the beginning (or else it gets stuck)
                                    break;
                            }
                        }

                        // Smooth Edges on Unity 4+
                        if (smoothEdges)
                        {
                            cubemap.SmoothEdges(smoothEdgesWidth);
                            cubemap.Apply();
                        }

                        // Create Cubemap Asset
                        string finalCubemapPath = pathCubemaps + "/" + sceneName + " - " + nodes[a].name + ".cubemap";
                        if (finalCubemapPath.Contains("//"))
                            finalCubemapPath = finalCubemapPath.Replace("//", "/");

                        AssetDatabase.CreateAsset(cubemap, finalCubemapPath);

                        // Set Linear Space if wanted
                        CMGenerate.SetLinear(cubemap, useLinearSpace);

                        // Free up memory to prevent memory leak
                        Resources.UnloadUnusedAssets();
                    }
                }

                CMGenerate.ToggleNodePreview(true); // Re-enable node preview

                AssetDatabase.Refresh();

                isTakingScreenshots = false;
                completedTakingScreenshots = true;
            }

            Debug.Log("CUBEMAPS GENERATED!");
        }

        IEnumerator MakeSnapshot(Cubemap c, CubemapFace face, CubemapNode node)
        {
            // We should only read the screen buffer after rendering is complete
            yield return new WaitForEndOfFrame();

            int width = Screen.width;
            int height = Screen.height;

            //Create the blank texture container
            Texture2D snapshot = new Texture2D(width, height, textureFormat, useMipMaps);
            snapshot.wrapMode = TextureWrapMode.Clamp;

            // Rectangle Area from the Camera
            Rect copyRect = new Rect((cam.pixelWidth * 0.5f) - (snapshot.width * 0.5f), (cam.pixelHeight * 0.5f) - (snapshot.height * 0.5f), snapshot.width, snapshot.height);

            //Read the current render into the texture container, snapshot
            snapshot.ReadPixels(copyRect, 0, 0, false);

            yield return null;

            snapshot.Apply();

            // Resize our Texture
            snapshot = CMGenerate.Scale(snapshot, nodeResolution, nodeResolution);

            // Write mirrored pixels from our Texture to Cubemap
            Color cubemapColor;
            for (int y = 0; y < nodeResolution; y++)
            {
                for (int x = 0; x < nodeResolution; x++)
                {
                    cubemapColor = snapshot.GetPixel(nodeResolution + x, (nodeResolution - 1) - y);
                    c.SetPixel(face, x, y, cubemapColor);
                }
            }
            c.Apply();

            // Optional PNG generation. Double-check it with overriding Node setting
            if (makePNG && node.allowGeneratePNG)
            {
                // Mirror the snapshot image for our PNG in order to be identical with the cubemap faces
                snapshot.SetPixels32(CMGenerate.MirrorColor32(c.GetPixels(face)));
                snapshot.Apply();

                // Convert to PNG file
                byte[] bytes = snapshot.EncodeToPNG();

                // Save the file			
                string path = Application.dataPath + "/" + pathCubemapPNG + "/" + sceneName + " - " + node.name + " - " + face.ToString() + ".png";
                //System.IO.File.WriteAllBytes(path, bytes); // deprecated because not available on Webplayer
                System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Create);
                System.IO.BinaryWriter bw = new System.IO.BinaryWriter(fs);
                bw.Write(bytes);
                bw.Close();
                fs.Close();

                // Fix compression state
                string finalImagePath = "Assets/" + pathCubemapPNG + "/" + sceneName + " - " + node.name + " - " + face.ToString() + ".png";
                if (finalImagePath.Contains("//"))
                    finalImagePath = finalImagePath.Replace("//", "/");

                AssetDatabase.Refresh(); // refresh necessary before we can use the textureimporter

                TextureImporter textureImporter = AssetImporter.GetAtPath(finalImagePath) as TextureImporter;
                if (textureImporter != null)
                {
                    textureImporter.textureFormat = TextureImporterFormat.RGB24;
                    AssetDatabase.ImportAsset(finalImagePath);
                }
            }

            // Delete our screenshot texture as clean up
            DestroyImmediate(snapshot);

            yield return null;
        }


        public void Generate_UnityPro()
        {
            sceneName = CMGenerate.GetSceneName(generateMethod); // Get scene name for naming purposes later on
            CMGenerate.ToggleNodePreview(false); // Disable node preview

            // Loop through each node for capture
            for (int a = 0; a < nodes.Length; a++)
            {
                // Make sure this Node is set to allow generation of either cubemaps or PNGs
                // Ignore the Allow parameter if we have no cubemap assigned yet at all
                if (nodes[a].allowCubemapGeneration || (!nodes[a].allowCubemapGeneration && nodes[a].cubemap == null))
                {
                    // Does node request a custom resolution?
                    if (nodes[a].overrideResolution)
                        resolution = nodes[a].resolution;

                    // Create or reference the Cubemap File that we will render into at path
                    // This helps prevent referenced Cubemaps from disappearing
                    string finalCubemapPath = pathCubemaps + "/" + sceneName + " - " + nodes[a].name + ".cubemap";
                    if (finalCubemapPath.Contains("//"))
                        finalCubemapPath = finalCubemapPath.Replace("//", "/");

                    Cubemap cubemap = new Cubemap(resolution, textureFormat, useMipMaps);
                    AssetDatabase.CreateAsset(cubemap, finalCubemapPath);

                    // Set Mip Map Bias
                    cubemap.mipMapBias = mipMapBias;

                    // Create Camera for generating
                    SetupCamera();

                    // Position Camera to Node
                    camT.position = nodes[a].transform.position;
                    camT.rotation = Quaternion.identity;

                    // render into cubemap		
                    cam.RenderToCubemap(cubemap);

                    // Smooth Edges on Unity 4.0+
                    if (smoothEdges)
                    {
                        cubemap.SmoothEdges(smoothEdgesWidth);
                        cubemap.Apply();
                    }

                    // Use Linear Space?
                    CMGenerate.SetLinear(cubemap, useLinearSpace);

                    // Extract PNG if allowed
                    if (makePNG && nodes[a].allowGeneratePNG)
                    {
                        CMGenerate.CubemapToPNG(cubemap, pathCubemapPNG);
                    }

                    AssetDatabase.Refresh();
                    Selection.activeObject = cubemap;

                    // Cleanup
                    DestroyImmediate(camObj);
                }
            }

            // Assign Cubemaps to Nodes (but not Users)
            // If you do not want this to happen, you should specify on your Node Override settings to prohibit Assignments
            bool assignedCubemaps = false;
            CMGenerate.AssignCubemapsToNodes(out assignedCubemaps);

            // Re-enable node preview
            CMGenerate.ToggleNodePreview(true);

            Debug.Log("CUBEMAPS GENERATED!");
            EditorUtility.DisplayDialog("Cubemaps generated!", "You can now proceed assigning your cubemaps to your objects.", "Yay!");

            // Destroy Generator
            DestroyImmediate(gameObject);
        }
#endif
    }
}