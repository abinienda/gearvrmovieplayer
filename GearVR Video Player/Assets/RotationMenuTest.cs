﻿using UnityEngine;
using System.Collections;

public class RotationMenuTest : MonoBehaviour {

    public TextMesh testMesh;

    public float idleTurnSpeed = 0.3f;
    public float swipeSpeedFactor = 0.25f;
    public bool turnRight = true;
    public float easeDamp = 3.0f;

    float currentTurnSpeed = 0.0f;

    float turnDirection = -1.0f;

    Vector2 currentTouchpadPosition;
    Vector2 lastTouchpadPosition;

    float currentSwipeSpeed = 0.0f;

	// Use this for initialization
	void Start () {

        currentTouchpadPosition = new Vector2(0.0f, 0.0f);
        lastTouchpadPosition = new Vector2(0.0f, 0.0f);

        if (!turnRight)
            turnDirection = 1.0f;
        
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButton(0))
        {
            currentTouchpadPosition = (Vector2)Input.mousePosition;
            //float swipeSpeed = CustomTouchpadEvents.Instance.currentSwipeSpeed.x * -1.0f;

            if (lastTouchpadPosition.x == 0.0f)
            {
                currentSwipeSpeed = 0.0f;
            }
            else
            {
                currentSwipeSpeed = (currentTouchpadPosition.x - lastTouchpadPosition.x) * swipeSpeedFactor;
            }

            currentTurnSpeed = currentSwipeSpeed;
            //testMesh.text = "current speed: " + currentTurnSpeed.ToString();
            float rotationAmount = currentSwipeSpeed * Time.deltaTime;


            transform.RotateAroundLocal(Vector3.up, rotationAmount);            

            if (rotationAmount > 0)
            {
                turnDirection = 1.0f;
            }
            else
            {
                turnDirection = -1.0f;
            }

            lastTouchpadPosition = currentTouchpadPosition;
        }
        else // after release lerp to idle speed
        {
            currentTurnSpeed = Mathf.Lerp(currentTurnSpeed, idleTurnSpeed * turnDirection, easeDamp * Time.deltaTime);
            //testMesh.text = "current speed: " + currentTurnSpeed.ToString();

            transform.Rotate(Vector3.up, currentTurnSpeed);

            currentTouchpadPosition = new Vector2(0.0f, 0.0f);
            lastTouchpadPosition = new Vector2(0.0f, 0.0f);
        } 
	}
}
