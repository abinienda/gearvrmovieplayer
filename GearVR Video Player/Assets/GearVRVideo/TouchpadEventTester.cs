﻿using UnityEngine;
using System.Collections;

public class TouchpadEventTester : MonoBehaviour {

    public TextMesh eventText;
    public TextMesh boolText;
    public TextMesh messageText;
    public TextMesh statusText;

	// Use this for initialization
	void Awaken () {
        
	}

    void OnEnable()
    {
      
        LogStatus("Connect events.");

        CustomTouchpadEvents.onHold += OnHoldEvent;
        CustomTouchpadEvents.onRelease += OnReleaseEvent;
        CustomTouchpadEvents.swipeLeft += LeftEvent;
        CustomTouchpadEvents.onTap += OnTapEvent;
        CustomTouchpadEvents.onDoubleTap += OnDoubleTapEvent;
    }

    void OnDisable()
    {
        CustomTouchpadEvents.onHold -= OnHoldEvent;
        CustomTouchpadEvents.onRelease -= OnReleaseEvent;
        CustomTouchpadEvents.swipeLeft -= LeftEvent;
        CustomTouchpadEvents.onTap -= OnTapEvent;
        CustomTouchpadEvents.onDoubleTap -= OnDoubleTapEvent;
    }
	// Update is called once per frame
	void Update () {

        if (CustomTouchpadEvents.Instance.isTapped)
            LogBool("is Tapped");
        if (CustomTouchpadEvents.Instance.isDoubleTapped)
            LogBool("is double Tapped");
        if (CustomTouchpadEvents.Instance.isHolding)
            LogOnMessage("isOnHold");
	}

    void LeftEvent()
    {
        LogEvent("Left event");
    }

    void OnTapEvent()
    {
        LogEvent("On tap received.");
    }

    void OnDoubleTapEvent()
    {
        LogEvent("On double tap received.");
    }

    void OnHoldEvent()
    {
        LogEvent("On hold received.");
    }

    void OnReleaseEvent()
    {
        LogEvent("On release received.");
    }
    
    /////

    void Left()
    {
        LogOnMessage("Left event");
    }

    void OnTap()
    {
        LogOnMessage("On tap received.");
    }

    void OnDoubleTap()
    {
        LogOnMessage("On double tap received.");
    }

    void OnHold()
    {
        LogOnMessage("On hold received.");
    }

    void OnRelease()
    {
        LogOnMessage("On release received.");
    }


    void LogEvent(string text)
    {
        eventText.text = "Event:" + text;
    }

    void LogBool(string text)
    {
        boolText.text = "Bool: " + text;
    }

    void LogOnMessage(string text)
    {
        messageText.text = "Message: " + text;
    }

    void LogStatus(string text)
    {
        statusText.text = "Status: " + text;
    }
}
