﻿using UnityEngine;
using System.Collections;

public class PVDAppDirector : MonoBehaviour {

    public GameObject flatVideoScene;
    public GameObject sphereVideoScene;
    public GameObject menuScene;

    public GearVRMoviePlayer player;    

    CustomScreenFader[] faders;

    public TextMesh debugText;

    public CustomTouchpadEvents touchpadEvents;

    public bool doDebug;

    float fadeTime;
    

	// Use this for initialization
	void Start () {

        if (touchpadEvents==null)
            touchpadEvents = (CustomTouchpadEvents)FindObjectOfType(typeof(CustomTouchpadEvents));

        if (player ==null)
            player = (GearVRMoviePlayer)FindObjectOfType(typeof(GearVRMoviePlayer));
        

        flatVideoScene.SetActive(false);
        sphereVideoScene.SetActive(false);
        menuScene.SetActive(true);	

        faders = GameObject.FindObjectsOfType<CustomScreenFader>();
        if (faders == null)
        {
            Log("No faders");
        }
        else
        {
            fadeTime = faders[0].fadeTime;
            //StartCoroutine(SceneChangeTester());

        }

        if (!doDebug)
        {
            debugText.text = "";
        }
	}

    IEnumerator SceneChangeTester(){
        yield return new WaitForSeconds(3.0f);
        FadeOut();
        yield return new WaitForSeconds(5.0f);
        FadeIn();
        yield return new WaitForSeconds(5.0f);
        FadeOut();
        yield return new WaitForSeconds(5.0f);
        FadeIn();
        yield return new WaitForSeconds(5.0f);

        yield return new WaitForSeconds(10.0f);
        Log("Start scene change tester.");
        yield return new WaitForSeconds(3.0f);
        StartCoroutine(ActivateCinemaScene());
        yield return new WaitForSeconds(6.0f);
        StartCoroutine(ActivateSphereScene());
        yield return new WaitForSeconds(6.0f);
        StartCoroutine(ActivateMenuScene());
    }

    void Update(){

        if (touchpadEvents != null)
        {
            if (touchpadEvents.isBackButton)
            { // jump back to menu
                Log("Go back to menu");
                
                if (menuScene.activeSelf == false)
                {
                    StartCoroutine(ActivateMenuScene()); // FIXME: activeSelf does not work???    
                }
                else
                {
                    Log("Menu scene is already active...");
                }

            }
        }
        
    }
	
    
    public void LoadMoviePlaneVideo(string fileName, bool isStereo)
    {
        StartCoroutine(LoadPlaneScene(fileName, isStereo));
    }

    IEnumerator LoadPlaneScene(string fileName, bool isStereo)
    {
        Log("Load plane movie:" + fileName);

                    
        StartCoroutine(ActivateCinemaScene());
        yield return new WaitForSeconds(fadeTime * 2.0f);

        
        Log("Flat screen selector:" + player.screenSelector.name);

        if (player.gameObject.activeSelf)
        {
            player.LoadFile(fileName);            
            if (isStereo)
            {
                player.screenSelector.SetStereoHorizontal();
            }
            else
            {
                player.screenSelector.SetMono();            
                
            }

            //yield return new WaitForSeconds(3.0f);
            player.Play();
        }
        else
        {
            Log("Flat player not active");
        }
        
    }

    public void LoadMovieSphereVideo(string fileName, bool isStereo)
    {
        StartCoroutine(LoadSphereScene(fileName, isStereo));
    }

    IEnumerator LoadSphereScene(string fileName, bool isStereo)
    {
        Log("Load sphere movie:" + fileName);

        
        StartCoroutine(ActivateSphereScene());
        yield return new WaitForSeconds(fadeTime*2.0f);

        Log("Sphere selector:" + player.screenSelector.name);

        if (player.gameObject.activeSelf)
        {
            player.LoadFile(fileName);            
            if (isStereo)
            {
                player.screenSelector.SetStereoHorizontal();
            }
            else
            {
                player.screenSelector.SetMono();
            }
            //yield return new WaitForSeconds(1.0f);
            player.Play();
        }
        else
        {
            Log("Sphere player not active.");
        }
    }

    IEnumerator ActivateMenuScene(){
        Log("Activate menu");
        FadeOut();
        yield return new WaitForSeconds(fadeTime);        
        flatVideoScene.SetActive(false);
        sphereVideoScene.SetActive(false);
        menuScene.SetActive(true);	
        FadeIn();
        //yield return new WaitForSeconds(fadeTime);
    }

    IEnumerator ActivateCinemaScene(){
        Log("Activate cinema scene");
        FadeOut();
        yield return new WaitForSeconds(fadeTime);
        menuScene.SetActive(false);
        Log("After menu deactivation");
        sphereVideoScene.SetActive(false);
        Log("After sphere deactivation");
        flatVideoScene.SetActive(true);
        Log("After flat activation");
        //flatPlayer.Stop();
        FadeIn();
        Log("Activation of cinema scene has ended.");
        //yield return new WaitForSeconds(fadeTime);
    }

     IEnumerator ActivateSphereScene(){
        Log("Activate sphere scene");
        FadeOut();
        yield return new WaitForSeconds(fadeTime);
        menuScene.SetActive(false);
        flatVideoScene.SetActive(false);
        sphereVideoScene.SetActive(true);
        
        //spherePlayer.Stop();
	    FadeIn();
        //yield return new WaitForSeconds(fadeTime);
    }

    void FadeIn(){
        foreach (CustomScreenFader fader in faders)
        {            
            fader.FadeIn();
        }
    }

    void FadeOut(){
        foreach (CustomScreenFader fader in faders)
        {            
            fader.FadeOut();
        }
    }


    void Log(string text)
    {
        if (doDebug && debugText != null)
        {
            debugText.text = text;
        }        
    }

}
