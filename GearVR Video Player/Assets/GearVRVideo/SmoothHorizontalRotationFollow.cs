﻿using UnityEngine;
using System.Collections;

public class SmoothHorizontalRotationFollow : MonoBehaviour {

    public GameObject followObject; // an object (e.g. logo) that should follow the horizontal orientation of the goalObject (e.g. Camera)
    public GameObject goalObject;   // the object with the goal rotation 

    public float followSpeed; // the speed at which the goalObject will be followed by the followObject

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {       
        
        Vector3 rotationGoalInEulers = new Vector3(0.0f, goalObject.transform.rotation.eulerAngles.y, 0.0f);

        Quaternion rotationGoalInQuaternion = Quaternion.Euler(rotationGoalInEulers);

        followObject.transform.rotation = Quaternion.Lerp(followObject.transform.rotation, rotationGoalInQuaternion, Time.deltaTime*followSpeed);       
        
	}
}
