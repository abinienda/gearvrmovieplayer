﻿using UnityEngine;
using System.Collections;
using System.IO;


public class SelectableCube : MonoBehaviour {

    public bool doDebug;

    public TextMesh debugText;

    CustomTouchpadEvents touchpadEvents;

    bool isSelected;
    bool isOnHold;
    bool isOnRelease;

    Vector3 scaleOrigin;
    Vector3 positionOrigin;

    string movieName;

    PVDAppDirector director;

    bool isPlane;
    bool isStereo;

    GameObject selectionEdge;

    GearVRMoviePlayer player;


    void Awaken()
    {
        
        
        //CustomTouchpadEvents.onHold += OnHold;
        //CustomTouchpadEvents.onRelease += OnRelease;
    }

    void OnDisable()
    {
        //CustomTouchpadEvents.onHold -= OnHold;
        //CustomTouchpadEvents.onRelease -= OnRelease;
    }

    void Start()        
    {
        Transform childTrafo = transform.GetChild(0);

        if (childTrafo != null)
        {
            selectionEdge = childTrafo.gameObject;
        }
        if (selectionEdge!=null)
            selectionEdge.SetActive(false);

        director = (PVDAppDirector)FindObjectOfType(typeof(PVDAppDirector));

        scaleOrigin = transform.localScale;
        positionOrigin = transform.localPosition;
        //Debug.Log(scaleOrigin);
        if (touchpadEvents==null)
            touchpadEvents = (CustomTouchpadEvents)FindObjectOfType(typeof(CustomTouchpadEvents));


        renderer.material = new Material(Shader.Find(" Diffuse"));
        renderer.material.SetTextureScale("_MainTex", new Vector2(1, 1));

        player = (GearVRMoviePlayer)FindObjectOfType(typeof(GearVRMoviePlayer));


       
        // load logos
        Texture2D tex = new Texture2D(512, 512);
        tex.LoadImage(File.ReadAllBytes(player.movieRootLocation + "preview_pics/" + gameObject.name + ".png"));        
        renderer.material.mainTexture = tex;

        DirectoryInfo dir = new DirectoryInfo(player.movieRootLocation);
        FileInfo[] info = dir.GetFiles("*.mp4");
        foreach (FileInfo f in info)
        {
            Log("check file: " + f.Name);
            string movieAppendix = "movie" + gameObject.name;            
            if (f.Name.Contains(movieAppendix))
            {
                movieName = f.Name;
                if (f.Name.Contains("stereo"))
                {
                    isStereo = true;
                }
                else if (f.Name.Contains("mono")){
                    isStereo = false;
                }
                else
                {
                    isStereo = false;
                }

                if (f.Name.Contains("flat")){
                    isPlane = true;
                }
                else if (f.Name.Contains("sphere"))
                {
                    isPlane = false;
                }
                else {
                    isPlane = true;
                }
                break;
            }
            else
            {
                movieName = "";
            }
        }

        if (!doDebug)
        {
            debugText.text = "";
        }
    }

    public void OnHold()
    {
        //Log("Holding cube:  " + gameObject.name);
        isOnHold = true;
        isOnRelease = false;
    }

    public void OnRelease()
    {
        //Log("Releasing cube:  " + gameObject.name);
        isOnHold = false;
        isOnRelease = true;
    }

    void Update()
    {
        
        if (isSelected)
        {
            if (CustomTouchpadEvents.Instance!= null)
            {
                if (CustomTouchpadEvents.Instance.isTapped)
                {
                    Log("Tapped.");
                    if (movieName != "")
                    {
                        if (isPlane)
                        {
                            if (director != null)
                                director.LoadMoviePlaneVideo(movieName, isStereo);
                            else
                                Log("no director found.");
                        }
                        else
                        {
                            if (director!=null){
                                Log("Load:" + movieName + " with stereo: " + isStereo);
                                director.LoadMovieSphereVideo(movieName, isStereo);
                            }
                                
                            else
                                Log("no director found.");
                        }
                    }
                    else
                    {
                        Log("Movie name is empty.");
                    }
                }
            }
            else
            {
                Log("No touchpad events found!");
            }
            
        }  
         
    }

    void Log(string text)
    {
        if (doDebug && debugText != null)
            debugText.text = text;
    }

    IEnumerator ClearText()
    {
        yield return new WaitForSeconds(0.5f);
        debugText.text = "";
    }

    void OnClick()
    {
        Log("I have been clicked:" + gameObject.name);
        
      
        
    }

    public void Select()
    {
        //Log("Select: " + gameObject.name);
        isSelected = true;

        //transform.localScale = new Vector3(scaleOrigin.x, scaleOrigin.y*1.05f, scaleOrigin.z);
        //transform.localPosition = new Vector3(positionOrigin.x + 0.25f, positionOrigin.y, positionOrigin.z);

        if (selectionEdge != null)
            selectionEdge.SetActive(true);

    }

    public void Deselect()
    {
        isSelected = false;
        
        //transform.localPosition = positionOrigin;
        if (selectionEdge != null)
            selectionEdge.SetActive(false);
    }

    
    void OnTriggerEnter(Collider other)
    {
        if (!isSelected)
        {
            Debug.Log("Trigger Enter" + gameObject.name);
            Select();
        }
        
        
    }

    void OnTriggerExit(Collider other)
    {
        if (isSelected)
        {
            Debug.Log("Trigger Exit " + gameObject.name);
            Deselect();
        }
        
        
    }
}
