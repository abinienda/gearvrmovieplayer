/************************************************************************************

Filename    :   GearVRMoviePlayer.cs
Content     :   Little GearVR video manager based on MoviePlayerSample.cs from the Unity SDK Examples
Created     :   February 14, 2015

************************************************************************************/

using UnityEngine;
using System.Collections;					// required for Coroutines
using System.Runtime.InteropServices;		// required for DllImport
using System;								// requred for IntPtr


/************************************************************************************
Usage:

	Place a simple textured quad surface with the correct aspect ratio in your scene.

	Add the MoviePlayerSample.cs script to the surface object.

	Modify it for the correct media location (currently hardcoded)

	Modify it to start when you want, instead of on the very first Update().

Implementation:

	In the GearVRMoviePlayer Awake() call, GetNativeTextureID() is called on 
	renderer.material.mainTexture.
	
	When the Oculus plugin gets the initialization event on the render thread, 
	it creates a new Android SurfaceTexture and Surface object in preparation 
	for receiving media. 

	When the game wants to start the video playing, it calls the StartVideoPlayerOnTextureId()
	script call, which creates an Android MediaPlayer java object, issues a 
	native plugin call to tell the native code which texture id to put the video
	on and return the Android Surface object to pass to MediaPlayer, then sets
	up the media stream and starts it.
	
	Every frame, right after VrApi::WarpSwap() is called by the native plugin,
	the	SurfaceTexture object is checked for updates.  If there is one, the
	target texId is re-created at the correct dimensions and format if it is
	the first frame, then the video image is rendered to it and mipmapped.  
	The following frame, instead of Unity drawing the image that was placed 
	on the surface in the Unity editor, it will draw the current video frame.

	It is important to note that the texture is actually replaced -- the original
	version is gone, and the video will now show up everywhere that texture was
	used, not just on sthe GameObject that ran the script.

	NOTE: The resolution of the movie texture is hard coded to 960x540 in MediaSurface.cpp.
	
************************************************************************************/

public class GearVRMoviePlayer : MonoBehaviour
{
    /*
    public GameObject StereoHorizontalLeftEye;
    public GameObject StereoHorizontalRightEye;
    public GameObject StereoVerticalLeftEye;
    public GameObject StereoVerticalRightEye;
    public GameObject MovieSurfaceMono;
    */

    [HideInInspector]
    public MovieScreenSelector screenSelector;

    public Texture movieTexture;

    public string movieRootLocation = "/storage/extSdCard/oculus/";
	private int nativeTextureID;

    public TextMesh debugText;

    public string idleSource = "/storage/extSdCard/oculus/Movies/idle.mp4";

    bool	startedVideo = false;

    bool doesCurrentMovieExits = false;
    string currentMovieName = "";

    public bool doDebug;

    /*
     
    public bool isMono
    {
        get
        {
            return MovieSurfaceMono.activeSelf;
        }
        set
        {
            if (value)
            {
                SetMono();
            }
        }
    }
    */


    #if (UNITY_ANDROID && !UNITY_EDITOR)
	    bool	videoPaused = false;
	    AndroidJavaObject	mediaPlayer = null;
    #endif

	/// <summary>
	/// Initialization of the movie surface
	/// </summary>
	void Awake() {
		Debug.Log( "Awake" );
		Application.targetFrameRate = 60;

        /*
		if ( renderer.material == null || renderer.material.mainTexture == null )
		{
			//Debug.LogError( "Can't GetNativeTextureID() for movie surface" );
            Log("Can't GetNativeTextureID() for movie surface");
			
		}*/

		// This aparently has to be done at Awake time, before
		// multithreaded rendering starts;

		if (movieTexture!=null){
            nativeTextureID = movieTexture.GetNativeTextureID();            
        }
        else
        {
            Log("Error: No movie texture -> Specify in inspector");
        }
            /*
        else {
            nativeTextureID = renderer.material.mainTexture.GetNativeTextureID();
        }*/
        

		Debug.Log ( "Movie Texture id: " + nativeTextureID );
        Log("Movie Texture id: " + nativeTextureID.ToString());
	}

	/// <summary>
	/// Auto-starts video playback
	/// </summary>
	void VideoPlayerInit() {
		if ( !startedVideo ) {
			startedVideo = true;
#if (UNITY_ANDROID && !UNITY_EDITOR)
			// This can only be done once multithreaded rendering is running
			mediaPlayer = StartVideoPlayerOnTextureId( nativeTextureID );
            if (mediaPlayer!=null){
                Log("mediaPlayer initiliazed ");
            }
            else {
                   Log("mediaPlayer not initiliazed ");
            }
            //StartCoroutine(StereoSphereTest());   
#endif
        }
	}

	void Start() {

        if (!doDebug)
        {
            debugText.text = "";
        }
        //SetMono();
        #if (UNITY_ANDROID && !UNITY_EDITOR)
		    // delay one frame because OVRCameraController initializes the render thread in Start()
            if (movieRootLocation == ""){
                movieRootLocation = "/storage/extSdCard/oculus/Movies/";
            }
		    Invoke( "VideoPlayerInit", 0.1f );      

            //StartCoroutine(StereoTest());        
            
#endif
         
    }

    void OnDisable()
    {
        Stop();
       
    }

    void OnEnable()
    {

    }

   


	/// <summary>
	/// Pauses video playback when the app loses or gains focus
	/// </summary>
	void OnApplicationPause( bool wasPaused ) {
		Debug.Log( "OnApplicationPause: " + wasPaused );
        Log("OnApplicationPause: " + wasPaused.ToString());
        #if (UNITY_ANDROID && !UNITY_EDITOR)
	        if ( mediaPlayer != null ) {
		        videoPaused = wasPaused;
		        mediaPlayer.Call( ( videoPaused ) ? "pause" : "start" );
	        }
        #endif
	}

	// This function returns an Android Surface object that is
	// bound to a SurfaceTexture object on an independent OpenGL texture id.
	// Each frame, before the TimeWarp processing, the SurfaceTexture is checked
	// for updates, and if one is present, the contents of the SurfaceTexture
	// will be copied over to the provided surfaceTexId and mipmaps will be 
	// generated so normal Unity rendering can use it.
	[DllImport("OculusPlugin")]
	private static extern IntPtr OVR_Media_Surface( int surfaceTexId );

    #if (UNITY_ANDROID && !UNITY_EDITOR)
	    /// <summary>
	    /// Set up the video player with the movie surface texture id
	    /// </summary>
	    AndroidJavaObject StartVideoPlayerOnTextureId( int textureId ) {		    
            Log("Setup VideoPlayer");

		    IntPtr  androidSurface = OVR_Media_Surface( textureId ); // call over OculusPluginDll

		    AndroidJavaObject mediaPlayer = new AndroidJavaObject( "android/media/MediaPlayer" );
           

		    // Can't use AndroidJavaObject.Call() with a jobject, must use low level interface
		    //mediaPlayer.Call( "setSurface", androidSurface );
		    IntPtr setSurfaceMethodId = AndroidJNI.GetMethodID( mediaPlayer.GetRawClass(),"setSurface","(Landroid/view/Surface;)V");
		    jvalue[] parms = new jvalue[1];
		    parms[0] = new jvalue();
		    parms[0].l = androidSurface;
		    AndroidJNI.CallObjectMethod( mediaPlayer.GetRawObject(), setSurfaceMethodId, parms );   
 
            //string filelocation = movieRootLocation + "VRoom/movie1.mp4";	
	
            //Stop();

            Log("Setup VideoPlayer succesful");
		    return mediaPlayer;
	    }
#endif


    IEnumerator StartTestSequence()
    {
        Log("Start Test Sequence");
        //SetMono();

        
        movieRootLocation = "/storage/extSdCard/oculus/Movies/";
        yield return new WaitForSeconds(0.5f);
        LoadFile("VRoom/movie1.mp4");        
        Play();
        yield return new WaitForSeconds(3.0f);        
        yield return new WaitForSeconds(2.0f);
        Stop();
        yield return new WaitForSeconds(5.5f);
        LoadFile("VRoom/movie2.mp4");
        yield return new WaitForSeconds(0.5f);
        Play();
        yield return new WaitForSeconds(10.0f);
        Stop();
        LoadFile("VRoom/movie3.mp4");
        yield return new WaitForSeconds(0.5f);
        Play();
        yield return new WaitForSeconds(10.0f);
        Pause();
        yield return new WaitForSeconds(2.0f);
        Play();
        yield return new WaitForSeconds(30.0f);
        Stop();
    }

    /*
    IEnumerator StereoTest()
    {
        movieRootLocation = "/storage/extSdCard/oculus/Movies/";
        if (ShowDebugInfo)
            Log.text = "Start Test Sequence";

        SetStereoHorizontal();
        LoadFile("avatar.mp4");
        Play();
        yield return new WaitForSeconds(30.0f);        
        Stop();
    }

     IEnumerator StereoSphereTest()
    {
        if (ShowDebugInfo)
            debugText.text = "Start Test Sequence";

        SetMono();
        LoadFile("360Videos/360 Tours Iceland.mp4");
        Play();
        SetTime(20.0f);
        yield return new WaitForSeconds(60.0f);
        SetStereoHorizontal();
        LoadFile("VrIntro/vrintro.mp4");
        Play();
        yield return new WaitForSeconds(60.0f);
        Stop();
    }

    */

    public bool LoadFile(string filename){

        currentMovieName = movieRootLocation + filename;
        #if (UNITY_ANDROID && !UNITY_EDITOR)
            Log("Try loading: " + filename);
            string filelocation = currentMovieName;		
            
            // check if file exists on android file system
            AndroidJavaObject fileObject = new AndroidJavaObject("java.io.File", filelocation);

            if (fileObject.Call<bool>("exists")){
                mediaPlayer.Call("reset");
                mediaPlayer.Call( "setDataSource", filelocation );
		        mediaPlayer.Call( "prepare" );
		        mediaPlayer.Call( "setLooping", false );	
	            //mediaPlayer.Call( "start" );

                Log("Loaded: " + filename);
                doesCurrentMovieExits = true;
                return true;
            }
            else 
            {
                Log("File \n\'" + filelocation + "\'\n does not exist!");
                doesCurrentMovieExits = false;
                return false;
            }

#endif
        return false;
}

    public void Play()
    {
        #if (UNITY_ANDROID && !UNITY_EDITOR)
            
            if (doesCurrentMovieExits){
                mediaPlayer.Call("start");
                Log("Play " + currentMovieName);
            }
            else{
                Log("Movie " + currentMovieName + " does not exist.");
            }
#endif
    }

    public void Pause()
    {
        #if (UNITY_ANDROID && !UNITY_EDITOR)
            mediaPlayer.Call( "pause" );
            Log("Pause");
#endif
    }

    public void Stop()
    {
        // stop and load idle movie to save performance overhead
        #if (UNITY_ANDROID && !UNITY_EDITOR)
            Log("Try to stop");   
            mediaPlayer.Call( "reset");            
            string locationBackup = movieRootLocation;
            mediaPlayer.Call( "setDataSource", idleSource );
            mediaPlayer.Call( "prepare" );
		    mediaPlayer.Call( "setLooping", false );	
            mediaPlayer.Call( "start" );
            Invoke("StopPlayer", 0.2f);             
                       
#endif
    }

    void StopPlayer()
    {
        #if (UNITY_ANDROID && !UNITY_EDITOR)
        mediaPlayer.Call("stop");
        Log("Stop"); 
        #endif
    }

    public float GetCurrentMovieLength()
    {
        #if (UNITY_ANDROID && !UNITY_EDITOR)
            int ms = mediaPlayer.Call<int>( "getDuration" );
            float seconds = (float) ms / 1000.0f;
            seconds = (float) System.Math.Round(seconds,1);
            Log("Duration is: " + seconds.ToString() + " seconds");
            return seconds;
#else
        return 0.0f;
        #endif
        

    }

    public float GetTime()
    {
#if (UNITY_ANDROID && !UNITY_EDITOR)
            int ms = mediaPlayer.Call<int>( "getCurrentPosition" );
            float seconds = (float) ms / 1000.0f;
            seconds = (float) System.Math.Round(seconds,1);
            Log("Current position is: " + seconds.ToString() + " seconds");
            return seconds;
#else
        return 0.0f;
#endif


    }

    public bool SetTime(float time)
    {
#if (UNITY_ANDROID && !UNITY_EDITOR)

            float movieLength = GetCurrentMovieLength();
            if (time > movieLength){
                Log("SetTime time is bigger than movie duration.");
                return false;
            }
            int timeInMS = (int) (time * 1000);
            
            mediaPlayer.Call( "seekTo", timeInMS );
            
            Log("Seek to: " + time.ToString() + " seconds");
            return true;
#else
        return false;
#endif


    }

    public void Log(string text)
    {
        Debug.Log(text); // show debug log anyway
        if (doDebug)
        {
            debugText.text = text;
        }
        
    }
}
