﻿using UnityEngine;
using System.Collections;

public class OculusPerformance : MonoBehaviour {

    public TextMesh text;

    
    

	// Use this for initialization
	void Start () {       

        if (text != null)
        {  
            StartCoroutine(ShowFrames());
        }
	}

    IEnumerator ShowFrames()
    {
        while (true)
        {
            float fps = OVRDevice.FrameRate;  

            string info = "FPS: " + fps.ToString() ;
            text.text = info;
            yield return new WaitForSeconds(0.5f);
		}
	}



}
