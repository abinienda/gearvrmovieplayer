﻿using UnityEngine;
using System.Collections;

public class Spin : MonoBehaviour
{
	public float speed = 10f;
	
	
	void Update ()
	{
		transform.Rotate(Vector3.up, speed * 1 * Time.deltaTime);
	}
}