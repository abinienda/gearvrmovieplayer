﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomTouchpadEvents : MonoBehaviour {

    // singleton
    private static CustomTouchpadEvents instance;

    // constructor
    private CustomTouchpadEvents() { }

    public delegate void TouchpadTapEventHandler();
    public static event TouchpadTapEventHandler onTap;
    public static event TouchpadTapEventHandler onDoubleTap;
    public static event TouchpadTapEventHandler onHold;
    public static event TouchpadTapEventHandler onRelease;

    public delegate void BackbuttonEventHandler();
    public static event BackbuttonEventHandler onBackButton;

    public delegate void TouchpadSwipeEventHandler();
    public static event TouchpadSwipeEventHandler swipeLeft;
    public static event TouchpadSwipeEventHandler swipeRight;
    public static event TouchpadSwipeEventHandler swipeUp;
    public static event TouchpadSwipeEventHandler swipeDown;

    public delegate void TouchpadAnalogSwipeEventHandler(float magnitude);
    public static event TouchpadAnalogSwipeEventHandler swipeLeftAnalog;
    public static event TouchpadAnalogSwipeEventHandler swipeRightAnalog;
    public static event TouchpadAnalogSwipeEventHandler swipeUpAnalog;
    public static event TouchpadAnalogSwipeEventHandler swipeDownAnalog;

    public delegate void TouchpadSwipeVector(Vector2 vector);
    public static event TouchpadSwipeVector swipeVector;

    public TextMesh eventDebugText;
    public TextMesh valueDebugText;

    public int filterSize;

    [HideInInspector]
    public bool isTapped, isDoubleTapped, isHolding, isReleased, left, right, up, down, isBackButton;

    List<Vector2> pastTouchpadPositions = new List<Vector2>();
    Vector2 currentTouchpadPosition = new Vector2(0.0f, 0.0f);
    Vector2 lastTouchpadPosition = new Vector2(0.0f, 0.0f);

    [HideInInspector]
    public Vector2 currentSwipeSpeed = new Vector2(0.0f, 0.0f);
    [HideInInspector]
    Vector2 currentSwipeAcceleration = new Vector2(0.0f, 0.0f);

    bool waitForTap = false;
    bool waitForDoubleTap = false;

    public float swipeMagnitudeThreshold;

    public bool doDebug;



    // Instance
    public static CustomTouchpadEvents Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType(typeof(CustomTouchpadEvents)) as CustomTouchpadEvents;
            }
            return instance;
        }
    }

	// Use this for initialization
	void Start () {
        isTapped = false;
        isDoubleTapped = false;
        isHolding = false;
        isReleased = true;
        left = false;
        right = false;
        up = false;
        down = false;
        isBackButton = false;

        //rotationDirection = 1.0f;
        //lastTouchpadPosition = new Vector2(0.0f, 0.0f);
        if (filterSize < 3) // the filter size must be at least 3 to calculate the acceleration
        {
            filterSize = 3;
        }

        if (swipeMagnitudeThreshold <= 0)
        {
            swipeMagnitudeThreshold = 5.0f;
        }

        if (!doDebug){
            eventDebugText.text = "";
            valueDebugText.text = "";
        }
            
	}
	
	// Update the list of the last filterSize touchpad positions and discover tap & swipe events
	void LateUpdate () {

        isTapped = false;
        isDoubleTapped = false;        
        left = false;
        right = false;
        up = false;
        down = false;
        isBackButton = false;
        
        currentTouchpadPosition = (Vector2) Input.mousePosition;
        //LogEvent(currentTouchpadPosition.ToString());

        if (Input.GetMouseButton(0))
        {
            if (pastTouchpadPositions.Count < filterSize)
            {
                pastTouchpadPositions.Add(currentTouchpadPosition);
            }
            else
            {
                pastTouchpadPositions.RemoveAt(0);
                pastTouchpadPositions.Add(currentTouchpadPosition);
            }

            CalculateDerivatives();
        }
        


        if (swipeVector != null)
        {
            if (Vector2.SqrMagnitude(currentSwipeSpeed) != 0.0f)
            {
                swipeVector(currentSwipeSpeed);
            }            
        }


        // discover analog and discrete swipe gestures

        if (currentSwipeSpeed.x != 0.0f && currentSwipeSpeed.y != 0.0f)
        {
            if (Mathf.Abs(currentSwipeSpeed.x) > Mathf.Abs(currentSwipeSpeed.y))
            {
                if (currentSwipeSpeed.x > 0)
                {
                    LogValue("Left.");
                    if (swipeLeftAnalog!=null)
                        swipeLeftAnalog(Mathf.Abs(currentSwipeSpeed.x));
                    if (Input.GetMouseButtonUp(0)) 
                    {                        
                        LogEvent("<swipeLeft>");
                        if (swipeLeft != null)
                        {
                            swipeLeft();
                            left = true;
                        }
                            
                    }                        
                }
                else
                {
                    LogValue("Right.");
                    if (swipeRightAnalog != null)
                        swipeRightAnalog(Mathf.Abs(currentSwipeSpeed.x));
                    if (Input.GetMouseButtonUp(0))
                    {
                        LogEvent("<swipeRight>");
                        if (swipeRight != null)
                        {
                            swipeRight();
                            right = true;
                        }
                            
                    } 
                    
                }
            }
            else
            {
                if (currentSwipeSpeed.y > 0)
                {
                    LogValue("Up.");
                    if (swipeUpAnalog != null)
                        swipeUpAnalog(Mathf.Abs(currentSwipeSpeed.y));
                    if (Input.GetMouseButtonUp(0))
                    {
                        LogEvent("<swipeUp>");
                        if (swipeUp != null)
                        {
                            swipeUp();
                            up = true;
                        }
                            
                    } 
                }
                else
                {
                    LogValue("Down.");
                    if (swipeDownAnalog != null)
                        swipeDownAnalog(Mathf.Abs(currentSwipeSpeed.y));
                    if (Input.GetMouseButtonUp(0))
                    {
                        LogEvent("<swipeDown>");
                        if (swipeDown != null)
                        {
                            swipeDown();
                            down = true;
                        }
                            
                    } 
                }
            }
        }
        else
        {
            //LogValue("Idle.");
        }
        

        
        // discover tap events
        if (Input.GetMouseButton(0))
        {
            
            if (onHold != null)
            {
                LogValue("OnHold");
                onHold();
                isHolding = true;
                isReleased = false;
                
            }
            else
            {
                LogValue("onHold is not subscribed to!!!");
            }

        }

        if (Input.GetMouseButtonDown(0))
        {
            if (!waitForTap)
            {
                waitForTap = true;
                StartCoroutine(StopWaitingForTap());
            }            
        }
        if (Input.GetMouseButtonUp(0) && Vector2.SqrMagnitude(currentSwipeSpeed) < swipeMagnitudeThreshold ) // if you are swiping then you are not tapping ;)
        {
            LogValue("Released");
            if (onRelease != null)
            {
                onRelease();
                isHolding = false;
                isReleased = true;
            }

            // clear swipe position, speed & acc because there is non after releasing the touchpad
            if (pastTouchpadPositions.Count > 0)
            {
                pastTouchpadPositions.Clear(); // clear filter when "releasing" the touchpad
                currentSwipeSpeed = Vector2.zero;
                currentSwipeAcceleration = Vector2.zero;
            }
                

            if (waitForTap && !waitForDoubleTap)
            {
                LogEvent("<onTap>");
                if (onTap != null)
                {
                    onTap();
                    
                }
                isTapped = true;
                waitForDoubleTap = true;
                StartCoroutine(StopWaitingForDoubleTap());
            }
            else if (waitForTap && waitForDoubleTap){
                LogEvent("<onDoubleTap>");

                if (onDoubleTap != null)
                {
                    onDoubleTap();
                    
                }
                isDoubleTapped = true;
                waitForTap = false;
                waitForDoubleTap = false;
            }
            
        }

        if (Input.GetMouseButtonDown(1))
        {
            LogEvent("<onBackButton>");
            if (onBackButton != null)
            {
                onBackButton();
            }
            isBackButton = true;
        }

        lastTouchpadPosition = currentTouchpadPosition;
	
	}

    void CalculateDerivatives() // calculate the first two derivateives: speed & acceleration of touchpad movement
    {
        if (pastTouchpadPositions.Count > 1){ // calculate speed
            List<Vector2> touchpadSpeeds = new List<Vector2>();

            for (int i=0; i< pastTouchpadPositions.Count -1; i++ ){
                Vector2 speed = pastTouchpadPositions[i+1] - pastTouchpadPositions[i];
                touchpadSpeeds.Add(speed);
            }            
            currentSwipeSpeed = CalculateVector2Average(touchpadSpeeds);

            if (touchpadSpeeds.Count > 1){ // calculate acceleration
                List<Vector2> touchpadAccelerations = new List<Vector2>();
                for (int i = 0; i < touchpadSpeeds.Count - 1; i++ )
                {
                    Vector2 speed = touchpadSpeeds[i + 1] - touchpadSpeeds[i];
                    touchpadAccelerations.Add(speed);
                }
                currentSwipeAcceleration = CalculateVector2Average(touchpadAccelerations);
            }
        }
    }


    Vector2 CalculateVector2Average(List<Vector2> vectorList) // returns the average Vector2 of a list of Vector2s
    {
        Vector2 vectorSum = new Vector2(0.0f, 0.0f);
        for (int i = 0; i < vectorList.Count; i++)
        {
            vectorSum += vectorList[i];
        }

        Vector2 average = new Vector2(vectorSum.x / vectorList.Count, vectorSum.y / vectorList.Count);
        return average;
    }



    IEnumerator StopWaitingForTap() // stop waiting for a "tap release"
    {
        yield return new WaitForSeconds(0.15f);
        waitForTap = false;
    }

    IEnumerator StopWaitingForDoubleTap() // stop waiting for "double tap release"
    {
        yield return new WaitForSeconds(0.6f);
        waitForDoubleTap = false;
    }

    void LogEvent(string text) // helper function to log events to a text mesh for debug purposes
    {
        if (doDebug && eventDebugText != null)
        {
            eventDebugText.text = text;
            //StartCoroutine(ClearLog());
        }
        
    }

    void LogValue(string text) // helper function to log values to a text mesh for debug purposes
    {
        if (doDebug && valueDebugText != null)
        {
            valueDebugText.text = text;
        }        
    }

    IEnumerator ClearLog() // clears the event log after 0.5 seconds
    {
        yield return new WaitForSeconds(0.5f);
        if (eventDebugText!=null)
            eventDebugText.text = "";
    }
}
