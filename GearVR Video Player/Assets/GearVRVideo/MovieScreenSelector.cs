﻿using UnityEngine;
using System.Collections;

public class MovieScreenSelector : MonoBehaviour {

    public GameObject StereoHorizontalLeftEye;
    public GameObject StereoHorizontalRightEye;
    public GameObject StereoVerticalLeftEye;
    public GameObject StereoVerticalRightEye;
    public GameObject MovieSurfaceMono;

    public GearVRMoviePlayer player;


    void OnEnable()
    {
        player.screenSelector = this;
    }

    void OnDisable()
    {
        player.Stop();
    }

	// Use this for initialization
	void Start () {
        if (player == null)
        {
            player = (GearVRMoviePlayer)FindObjectOfType(typeof(GearVRMoviePlayer));
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool SetStereoVertical()
    {
        if (StereoVerticalLeftEye != null && StereoVerticalRightEye != null)
        {
            StereoVerticalLeftEye.SetActive(true);
            StereoVerticalRightEye.SetActive(true);

            if (MovieSurfaceMono != null)
            {
                MovieSurfaceMono.SetActive(false);
            }
            if (StereoHorizontalLeftEye != null)
            {
                StereoHorizontalLeftEye.SetActive(false);
            }
            if (StereoHorizontalRightEye != null)
            {
                StereoHorizontalRightEye.SetActive(false);
            }
            //DebugText("Set to stereo vertical.");
            return true;
        }
        return false;
    }

    public bool SetStereoHorizontal()
    {
        if (StereoHorizontalLeftEye != null && StereoVerticalRightEye != null)
        {
            StereoHorizontalLeftEye.SetActive(true);
            StereoHorizontalRightEye.SetActive(true);

            if (MovieSurfaceMono != null)
            {
                MovieSurfaceMono.SetActive(false);
            }
            if (StereoVerticalLeftEye != null)
            {
                StereoVerticalLeftEye.SetActive(false);
            }
            if (StereoVerticalRightEye != null)
            {
                StereoVerticalRightEye.SetActive(false);
            }
            //DebugText("Set to stereo horizontal.");
            return true;
        }
        else
        {
            return false;
        }

    }
    public bool SetMono()
    {
        if (MovieSurfaceMono != null)
        {
            MovieSurfaceMono.SetActive(true);
            if (StereoHorizontalLeftEye != null)
            {
                StereoHorizontalLeftEye.SetActive(false);
            }
            if (StereoHorizontalRightEye != null)
            {
                StereoHorizontalRightEye.SetActive(false);
            }
            if (StereoVerticalLeftEye != null)
            {
                StereoVerticalLeftEye.SetActive(false);
            }
            if (StereoVerticalRightEye != null)
            {
                StereoVerticalRightEye.SetActive(false);
            }
            //DebugText("Set to mono.");
            return true;
        }
        else
        {
            return false;
        }

    }
}
