﻿using UnityEngine;
using System.Collections;

public class PaulVanDykeDirector : MonoBehaviour {

    public GameObject[] pvdLogos;
    public GearVRMoviePlayer player;
    public string movieRootLocation = "/storage/extSdCard/oculus/movies/";
    public string movieName = "VRoom/pvd.mp4";

	// Use this for initialization
	void Start () {

        if (player != null)
        {  
            StartCoroutine(StartMovie());
        }
        
        StartCoroutine(RotateLogos());

	}

    IEnumerator StartMovie()
    {
        yield return new WaitForSeconds(0.5f);
        player.movieRootLocation = movieRootLocation;
        if (player.LoadFile(movieName))
        {
            //player.SetMono();
            player.Play();
            float duration = player.GetCurrentMovieLength();
            yield return new WaitForSeconds(duration + 1.0f);        
        }
        else {
            yield return new WaitForSeconds(5.0f);
        }
        Application.Quit();        
    }

    IEnumerator RotateLogos()
    {
        yield return new WaitForSeconds(2.0f);
        while (true)
        {
            Debug.Log("Loop");
            foreach (GameObject logo in pvdLogos)
            {
                if (logo != null)
                {
                    LeanTween.rotateAroundLocal(logo, Vector3.up,  360.0f, 5.0f).setEase(LeanTweenType.easeInOutCubic);
                }
                else
                {
                    Debug.Log("Logo is null");
                }
            }
            yield return new WaitForSeconds(10.0f);
        }
    }
}
