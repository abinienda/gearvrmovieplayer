﻿using UnityEngine;
using System.Collections;

public class TestReceiver : MonoBehaviour {

    public TextMesh textMesh;


   	// Use this for initialization
	void OnEnable () {
        TestEvent.onSpace += OnSpace;
        TestEvent.onEnter += OnEnter;
	}

    void OnDisable()
    {
        TestEvent.onSpace -= OnSpace;
        TestEvent.onEnter -= OnEnter;
    }
	
	// Update is called once per frame
	void OnSpace () {
        textMesh.text = "OnSpace was received.";
        Debug.Log("OnSpace was received.");
	}

    void OnEnter()
    {
        Debug.Log("OnEnter was received.");
    }


}
