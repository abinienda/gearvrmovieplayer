﻿using UnityEngine;
using System.Collections;

public class TestEvent : MonoBehaviour {

    public delegate void MyEventHandler();
    public static event MyEventHandler onSpace;

    public delegate void MySecondEventHanlder();
    public static event MySecondEventHanlder onEnter;

	// Use this for initialization
	void Start () {
        StartCoroutine(SendEvent());
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (onSpace != null)
                onSpace();
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            if (onEnter != null)
                onEnter();
        }
	
	}

    IEnumerator SendEvent()
    {
        yield return new WaitForSeconds(1.0f);
        if (onSpace != null)
            onSpace();
    }
}
